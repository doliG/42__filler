/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 14:05:36 by glodi             #+#    #+#             */
/*   Updated: 2019/01/11 19:20:18 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		main(void)
{
	t_game	game;
	char	*line;
	int		best_x;
	int		best_y;

	if (get_next_line(0, &line) < 1
			|| (line[0] == '$' && !set_player(&game, line)))
		return (error_parsing());
	while (get_next_line(0, &line))
	{
		best_x = 0;
		best_y = 0;
		if (!process_parsing(&game, line))
			return (error_parsing());
		set_heat_map(&game);
		process_algo(&game, &best_x, &best_y);
		ft_printf("%d %d\n",
				(best_y - game.p_y_offset) % game.b_y_size,
				(best_x - game.p_x_offset) % game.b_x_size);
		reset_game(&game);
	}
	return (0);
}

void	reset_game(t_game *game)
{
	// int i;

	// i = 0;
	// while (i < game->b_y_size)
	// {
	// 	ft_memdel((void **)&game->heat_map[i]);
	// 	i++;
	// }
	// ft_memdel((void **)&game->heat_map);
	ft_arrfree(&game->piece);
	if (game->old_board)
		ft_arrfree(&game->old_board);
	game->old_board = game->board;
	game->p_x_size = 0;
	game->p_y_size = 0;
	game->p_x_offset = 0;
	game->p_y_offset = 0;
}

void	ft_arrfree(char ***arr)
{
	int i;

	i = 0;
	if (!*arr)
		return ;
	while ((*arr)[i])
	{
		free((*arr)[i]);
		(*arr)[i] = NULL;
		i++;
	}
	free(*arr);
	*arr = NULL;
}
