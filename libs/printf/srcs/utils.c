/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/30 15:16:20 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:46:36 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		pf_greatest(int size, ...)
{
	va_list arg;
	int		greatest;
	int		tmp;

	va_start(arg, size);
	greatest = 0;
	while (size--)
	{
		tmp = va_arg(arg, int);
		if (tmp > greatest)
			greatest = tmp;
	}
	return (greatest);
}
