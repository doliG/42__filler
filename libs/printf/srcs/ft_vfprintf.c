/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vfprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 12:08:42 by glodi             #+#    #+#             */
/*   Updated: 2018/12/12 15:45:47 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_vfprintf(FILE *stream, char *format, va_list ap)
{
	t_buff	buff;
	t_flags	flags;
	t_conv	conv;

	ft_bzero(&buff, sizeof(buff));
	buff.fd = stream->_file;
	while (*format)
	{
		if (*format == '%')
		{
			format++;
			if (!*format)
				break ;
			ft_bzero(&flags, sizeof(flags));
			ft_bzero(&conv, sizeof(conv));
			format = parse_flags(format, &flags);
			if (flags.conversion)
				to_buffer(&buff, &flags, &conv, ap);
		}
		else
			format += add_char(&buff, *format);
	}
	return (write_buffer(&buff));
}
