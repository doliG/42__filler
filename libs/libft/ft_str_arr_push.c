/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_arr_push.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 17:28:35 by glodi             #+#    #+#             */
/*   Updated: 2018/05/04 13:21:07 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_str_arr_push(char **arr, char *s)
{
	size_t	arr_len;
	size_t	i;
	char	**tmp;

	if (arr && s)
	{
		arr_len = 0;
		while (arr[arr_len])
			arr_len++;
		if (!(tmp = (char **)malloc((arr_len + 2) * sizeof(char *))))
			return (NULL);
		i = 0;
		while (i < arr_len)
		{
			tmp[i] = arr[i];
			i++;
		}
		tmp[arr_len] = s;
		tmp[arr_len + 1] = NULL;
		free(arr);
		arr = tmp;
		return (tmp);
	}
	return (NULL);
}
