/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/24 17:42:56 by glodi             #+#    #+#             */
/*   Updated: 2018/12/17 16:00:07 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	get_next_line(const int fd, char **line)
{
	static char	*cache[OPEN_MAX + 1];
	int			ret;
	char		*tmp;
	char		*lf;

	if (fd < 0 || !line || fd > OPEN_MAX)
		return (-1);
	if (!cache[fd])
		cache[fd] = ft_strnew(0);
	ret = read_until_lf(fd, cache);
	if (ret == 1 && (lf = ft_strchr(cache[fd], '\n')))
	{
		*line = ft_strsub(cache[fd], 0, lf - cache[fd]);
		tmp = cache[fd];
		cache[fd] = ft_strdup(lf + 1);
		ft_strdel(&tmp);
	}
	else if (ret == 1)
	{
		*line = ft_strdup(cache[fd]);
		ft_strdel(&cache[fd]);
	}
	return (ret);
}

int	read_until_lf(const int fd, char *cache[fd])
{
	int		ret;
	char	buff[GNL_BUFF_SIZE + 1];
	char	*tmp;

	while (!(ft_strchr(cache[fd], '\n'))
		&& (ret = read(fd, buff, GNL_BUFF_SIZE)) > 0)
	{
		buff[ret] = '\0';
		tmp = cache[fd];
		cache[fd] = ft_strjoin(cache[fd], buff);
		ft_strdel(&tmp);
	}
	if (ret == -1)
		return (-1);
	else if (ret == 0 && !cache[fd][0])
		return (0);
	return (1);
}
