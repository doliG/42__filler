/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heat_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/02 16:12:11 by glodi             #+#    #+#             */
/*   Updated: 2019/01/08 17:53:04 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		set_heat_map(t_game *game)
{
	int x;
	int y;

	if (!game->heat_map)
	{
		if (!(game->heat_map = calloc(sizeof(int *), game->b_y_size)))
			return (1);
		y = -1;
		while (++y < game->b_y_size)
			if (!(game->heat_map[y] = calloc(sizeof(int), game->b_x_size)))
				return (0);
	}
	y = 0;
	while (y < game->b_y_size)
	{
		x = 0;
		while (x < game->b_x_size)
		{
			if (game->board[y][x] == game->opponent)
				update_heat_map(game, x, y);
			x++;
		}
		y++;
	}
	return (1);
}

int		update_heat_map(t_game *game, int ox, int oy)
{
	int		x;
	int		y;
	int		tmp;

	y = 0;
	if (game->old_board
		&& game->old_board[oy][ox] == game->board[oy][ox]) // Do not update if already known
		return (0);
	while (y < game->b_y_size)
	{
		x = 0;
		while (x < game->b_x_size)
		{
			if (game->board[y][x] != game->opponent
				&& game->board[y][x] != game->me)
			{
				tmp = my_sqrt((ox - x) * (ox - x) + (oy - y) * (oy - y));
				if (tmp < game->heat_map[y][x] || game->heat_map[y][x] == 0)
					game->heat_map[y][x] = tmp;
			}
			x++;
		}
		y++;
	}
	return (0);
}
