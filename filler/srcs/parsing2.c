/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 12:35:46 by glodi             #+#    #+#             */
/*   Updated: 2019/01/11 13:16:11 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

/*
** Return 1 if success, 0 if it fails.
*/

int		set_board(t_game *game, char *line)
{
	char	**words;

	if (!(words = ft_strsplit(line, ' '))
			|| !words[0] || !words[1] || !words[2])
	{
		ft_arrfree(&words);
		ft_strdel(&line);
		return (0);
	}
	game->b_y_size = ft_atoi(words[1]);
	game->b_x_size = ft_atoi(words[2]);
	ft_arrfree(&words);
	ft_strdel(&line);
	if (game->b_y_size == 0 || game->b_x_size == 0)
		return (0);
	get_next_line(0, &line);
	ft_strdel(&line);
	game->board = get_board(game, line);
	if (!game->board)
		return (0);
	return (1);
}

/*
** Return an array of pointer which is null terminated or NULL if it fails
*/

char	**get_board(t_game *game, char *line)
{
	int		i;
	char	**board;
	char	**words;

	i = 0;
	if (!(board = ft_memalloc(sizeof(char *) * (game->b_y_size + 1))))
		return (0);
	while (i < game->b_y_size && get_next_line(0, &line))
	{
		if (!(words = ft_strsplit(line, ' ')) || !words[0] || !words[1])
			return (exit_board(board, words, line));
		if (!(board[i] = ft_strdup(words[1]))
				|| (int)ft_strlen(words[1]) != game->b_x_size)
			return (exit_board(board, words, line));
		ft_strdel(&line);
		ft_arrfree(&words);
		i++;
	}
	board[i] = NULL;
	return (board);
}

/*
** Return 1 if success, 0 if it fails.
*/

int		set_piece(t_game *game, char *line)
{
	int		i;
	char	**words;

	if (!(words = ft_strsplit(line, ' ')) || !words[1] || !words[2])
		return ((int)exit_piece(game->board, words, line));
	game->p_y_size = ft_atoi(words[1]);
	game->p_x_size = ft_atoi(words[2]);
	ft_arrfree(&words);
	ft_strdel(&line);
	if (!(game->piece = get_piece(game, line))
			|| game->p_y_size <= 0 || game->p_x_size <= 0)
		return (0);
	game->p_x_offset = INT_MAX;
	game->p_y_offset = 0;
	i = -1;
	while (game->piece[++i])
		if (ft_strchr(game->piece[i], '*') &&
			ft_strchr(game->piece[i], '*') - game->piece[i] < game->p_x_offset)
			game->p_x_offset = ft_strchr(game->piece[i], '*') - game->piece[i];
	i = -1;
	while (game->piece[++i] && !ft_strchr(game->piece[i], '*'))
		game->p_y_offset++;
	return (1);
}

/*
** Return an array of pointer which is null terminated
*/

char	**get_piece(t_game *game, char *line)
{
	int		i;
	char	**piece;

	if (!(piece = ft_memalloc(sizeof(char *) * (game->p_y_size + 1))))
		return (0);
	i = -1;
	while (++i < game->p_y_size && get_next_line(0, &line))
	{
		piece[i] = line;
		if ((int)ft_strlen(piece[i]) != game->p_x_size)
			return (exit_piece(game->board, piece, NULL));
	}
	piece[i] = NULL;
	if (i < game->p_y_size)
		return (exit_piece(game->board, piece, NULL));
	return (piece);
}
