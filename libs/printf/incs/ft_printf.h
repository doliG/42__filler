/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 16:25:09 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:38:34 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdio.h>
# include <unistd.h>
# include "libft.h"
# include "ft_printf_struct.h"

# define STR_BASE_LO "0123456789abcdef"
# define STR_BASE_UP "0123456789ABCDEF"

int		ft_printf(const char *format, ...);
int		ft_vfprintf(FILE *stream, char *format, va_list ap);

/*
** Parsing
*/

char	*parse_flags(char *format, t_flags *flags);

/*
** Buffer func
*/

void	to_buffer(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
size_t	add_char(t_buff *buff, char c);
size_t	add_str(t_buff *buff, char *s);
size_t	write_buffer(t_buff *buff);

void	fill_field_width(t_buff *buff, t_flags *flags);

/*
** Conversion
*/

void	cast_ap(t_flags *flags, t_conv *conv, va_list ap);
void	ucast_ap(t_flags *flags, t_conv *conv, va_list ap);

void	pf_lltoa_base(t_flags *flags, t_conv *conv, int base);
void	pf_ulltoa_base(t_flags *flags, t_conv *conv, int base);
void	pf_ftoa_base(t_conv *conv, int base);

void	pf_handle_dec(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
void	pf_handle_udec(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
void	pf_handle_oct(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
void	pf_handle_hex(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
void	pf_handle_ptr(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
void	pf_handle_str(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
void	pf_handle_char(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
void	pf_handle_flt(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap);
void	pf_handle_unknown(t_buff *buff, t_flags *flags, t_conv *conv);

/*
** Utils
*/

int		pf_greatest(int size, ...);

#endif
