/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   oct.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 16:17:02 by glodi             #+#    #+#             */
/*   Updated: 2018/12/12 15:28:35 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	oct_sucks(t_buff *buff, t_flags *flags)
{
	size_t	end;

	end = 0;
	if (flags->justify_left)
	{
		fill_field_width(buff, flags);
		end = buff->curr_idx;
		buff->curr_idx -= flags->min_field_width;
	}
	else if (flags->min_field_width)
	{
		fill_field_width(buff, flags);
		buff->curr_idx--;
	}
	add_char(buff, '0');
	if (end > buff->curr_idx)
		buff->curr_idx = end;
}

static void	oct_to_buffer(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	while (to_write > conv->partial_len)
		to_write -= add_char(buff, conv->fill);
	if (to_write > 0 && flags->alternate_form)
		to_write -= add_char(buff, '0');
	while (to_write > conv->n_len)
		to_write -= add_char(buff, '0');
	while (to_write > 0)
		to_write -= add_char(buff, conv->n_conv[i++]);
}

static void	oct_left_to_buffer(t_buff *buff, t_flags *flags, t_conv *conv)
{
	int	to_write;
	int i;

	to_write = conv->total_len;
	i = 0;
	if (flags->alternate_form)
		to_write -= add_char(buff, '0');
	while (conv->partial_len > conv->n_len + !!flags->alternate_form)
	{
		to_write -= add_char(buff, '0');
		conv->partial_len--;
	}
	while (to_write && conv->n_conv[i])
	{
		to_write -= add_char(buff, conv->n_conv[i]);
		i++;
	}
	while (to_write > 0)
		to_write -= add_char(buff, ' ');
}

void		pf_handle_oct(t_buff *buff, t_flags *flags, t_conv *conv,
		va_list ap)
{
	ucast_ap(flags, conv, ap);
	pf_ulltoa_base(flags, conv, 8);
	conv->fill = flags->padd_with_zero && !flags->justify_left
		&& !(flags->precision != -1) ? '0' : ' ';
	conv->n_len = ft_strlen(conv->n_conv);
	if (conv->n == 0 && flags->alternate_form && flags->precision == 0)
		return (oct_sucks(buff, flags));
	if (conv->n == 0 && flags->precision == 0)
		conv->n_len = 0;
	conv->partial_len = pf_greatest(2, flags->precision, conv->n_len +
			!!flags->alternate_form);
	conv->total_len = pf_greatest(3, flags->min_field_width,
			flags->precision, conv->n_len + !!flags->alternate_form);
	if (flags->alternate_form && conv->n == 0)
	{
		conv->partial_len--;
		conv->total_len--;
	}
	if (flags->justify_left)
		oct_left_to_buffer(buff, flags, conv);
	else
		oct_to_buffer(buff, flags, conv);
}
