# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: glodi <glodi@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/17 11:35:41 by glodi             #+#    #+#              #
#    Updated: 2019/01/11 19:04:31 by glodi            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Variables
NAME = glodi.filler

LIBS_DIR = libs
LIBS = $(LIBS_DIR)/libftprintf.a

DIR_SRCS = filler/srcs/
DIR_OBJS = filler/objs/
DIR_INCS = filler/incs/

FILES_FILLER = main.c parsing.c parsing2.c heat_map.c algo.c utils.c

SRCS_FILLER = $(addprefix $(DIR_SRCS), $(FILES_FILLER))
OBJS_FILLER = $(addprefix $(DIR_OBJS), $(FILES_FILLER:.c=.o))

CC = gcc
CFLAGS = -Wall -Wextra -Werror -Ofast

INC_COMP = -I$(DIR_INCS) -I$(LIBS_DIR)/libft -I$(LIBS_DIR)/printf/incs

DEPS = Makefile $(DIR_INCS)/filler.h 

RM = rm -f
RM_DIR = rm -rf

MAKE = make -C

# Rules
all: $(LIBS) $(NAME)

$(LIBS):
	@$(MAKE) $(LIBS_DIR)

$(DIR_OBJS)%.o: $(DIR_SRCS)%.c $(DEPS)
	@printf "\033[K\r"
	@printf "Compiling %s" $@
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)

$(DIR_OBJS):
	@mkdir -p $@

$(NAME): $(DIR_OBJS) $(OBJS_FILLER) $(LIBS)
	@$(CC) $(CFLAGS) $(OBJS_FILLER) $(INC_COMP) -o $(NAME) -Llibs -lftprintf
	@printf "\033[K\r"
	@printf "\e[32m[OK]\e[39m \e[4m$(NAME)\e[24m successfuly created.\n"

clean:
	@$(MAKE) $(LIBS_DIR) clean
	@$(RM_DIR) $(DIR_OBJS)
	
fclean: clean
	@$(MAKE) $(LIBS_DIR) fclean
	@$(RM) $(NAME)

re: fclean all

test: $(NAME)
	$(CC) $(CFLAGS) -g main.c $(NAME) $(INC_COMP)

norme: $(DIR_INCS) $(DIR_SRCS)
	norminette $^ | grep -B 1 "Warning\|Error" || true

.PHONY: all clean fclean re norme test
