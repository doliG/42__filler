/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/02 14:18:33 by glodi             #+#    #+#             */
/*   Updated: 2019/01/08 17:48:11 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		process_parsing(t_game *game, char *line)
{
	if (!ft_strstr(line, "Plateau") || !set_board(game, line))
		return (0);
	if (get_next_line(0, &line) < 1)
		return (0);
	if (!ft_strstr(line, "Piece") || !set_piece(game, line))
		return (0);
	return (1);
}

int		set_player(t_game *game, char *line)
{
	int		i;
	int		nb;

	i = 0;
	nb = 0;
	while (line[i] && !ft_isdigit(line[i]))
		i++;
	if (line[i])
		nb = ft_atoi(&line[i]);
	ft_strdel(&line);
	if (nb == 1)
	{
		game->me = 'O';
		game->opponent = 'X';
		return (1);
	}
	else if (nb == 2)
	{
		game->me = 'X';
		game->opponent = 'O';
		return (1);
	}
	return (0);
}
