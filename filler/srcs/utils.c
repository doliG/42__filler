/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/11 13:12:39 by glodi             #+#    #+#             */
/*   Updated: 2019/01/11 13:15:17 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

/*
** Utils functions below
*/

/*
** Reimplementation of sqrt returning approximative root rounded to lower int
*/

int		my_sqrt(int nb)
{
	long a;
	long b;

	a = 0x8000;
	b = 0x8000;
	while (1)
	{
		if (b * b > nb)
			b ^= a;
		a >>= 1;
		if (a == 0)
			return (b);
		b |= a;
	}
}

int		error_parsing(void)
{
	ft_printf(C_YEL "[PARSER] " C_RES "Error while parsing.\n");
	return (1);
}

char	**exit_board(char **board, char **words, char *line)
{
	ft_arrfree(&board);
	ft_arrfree(&words);
	ft_strdel(&line);
	return (0);
}

char	**exit_piece(char **board, char **piece, char *line)
{
	ft_arrfree(&board);
	ft_arrfree(&piece);
	ft_strdel(&line);
	return (0);
}
