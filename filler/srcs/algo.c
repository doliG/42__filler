/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 12:02:41 by glodi             #+#    #+#             */
/*   Updated: 2019/01/11 12:06:45 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	process_algo(t_game *game, int *best_x, int *best_y)
{
	int	x;
	int	y;
	int	heat;
	int	best_heat;

	best_heat = INT_MAX;
	y = -1;
	while (++y < game->b_y_size)
	{
		x = -1;
		while (++x < game->b_x_size)
		{
			heat = get_heat(game, x, y);
			if (heat != 0 && heat < best_heat && is_putable(game, x, y))
			{
				*best_x = x;
				*best_y = y;
				best_heat = heat;
			}
		}
	}
	if (*best_x + game->p_x_size >= game->b_x_size)
		*best_x -= game->b_x_size;
	if (*best_y + game->p_y_size >= game->b_y_size)
		*best_y -= game->b_y_size;
}

int		is_putable(t_game *game, int x, int y)
{
	int	x1;
	int	y1;
	int	collision;

	y1 = -1;
	collision = 0;
	while (++y1 < game->p_y_size - game->p_y_offset)
	{
		x1 = 0;
		while (x1 < game->p_x_size - game->p_x_offset)
		{
			if (game->piece[y1 + game->p_y_offset][x1 + game->p_x_offset]\
					== '*')
			{
				if (game->board[(y + y1) % game->b_y_size]\
						[(x + x1) % game->b_x_size] == game->opponent)
					return (0);
				if (game->board[(y + y1) % game->b_y_size]\
						[(x + x1) % game->b_x_size] == game->me)
					collision++;
			}
			x1++;
		}
	}
	return (collision == 1);
}

int		get_heat(t_game *game, int x, int y)
{
	int	x1;
	int	y1;
	int heat;

	heat = 0;
	y1 = 0;
	while (y1 < game->p_y_size - game->p_y_offset)
	{
		x1 = 0;
		while (x1 < game->p_x_size - game->p_x_offset)
		{
			if (game->piece[y1 + game->p_y_offset][x1 + game->p_x_offset]\
					== '*')
				heat += game->heat_map[(y + y1) % game->b_y_size]\
						[(x + x1) % game->b_x_size];
			x1++;
		}
		y1++;
	}
	return (heat);
}
