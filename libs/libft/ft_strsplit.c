/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 10:37:33 by glodi             #+#    #+#             */
/*   Updated: 2018/05/04 13:57:27 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_strsplit(char const *s, char c)
{
	size_t	begin;
	size_t	end;
	char	*tmp;
	char	**res;

	if (!s || !c || !(res = (char **)malloc(1 * sizeof(char *))))
		return (NULL);
	res[0] = NULL;
	begin = 0;
	while (s[begin])
	{
		while (s[begin] == c)
			begin++;
		end = begin;
		while (s[end] && s[end] != c)
			end++;
		if (begin != end)
		{
			tmp = ft_strsub(s, begin, end - begin);
			if (!(res = ft_str_arr_push(res, tmp)))
				return (NULL);
		}
		begin = end;
	}
	return (res);
}
