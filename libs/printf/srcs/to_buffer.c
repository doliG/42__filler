/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   to_buffer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 10:36:56 by glodi             #+#    #+#             */
/*   Updated: 2018/12/11 19:39:41 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	to_buffer(t_buff *buff, t_flags *flags, t_conv *conv, va_list ap)
{
	if ('d' == flags->conversion || 'i' == flags->conversion)
		pf_handle_dec(buff, flags, conv, ap);
	else if ('o' == flags->conversion)
		pf_handle_oct(buff, flags, conv, ap);
	else if ('u' == flags->conversion)
		pf_handle_udec(buff, flags, conv, ap);
	else if ('x' == flags->conversion || 'X' == flags->conversion)
		pf_handle_hex(buff, flags, conv, ap);
	else if ('s' == flags->conversion)
		pf_handle_str(buff, flags, conv, ap);
	else if ('c' == flags->conversion)
		pf_handle_char(buff, flags, conv, ap);
	else if ('p' == flags->conversion)
		pf_handle_ptr(buff, flags, conv, ap);
	else if ('f' == flags->conversion)
		pf_handle_flt(buff, flags, conv, ap);
	else
		pf_handle_unknown(buff, flags, conv);
}

/*
** Usefull buff functions
*/

size_t	write_buffer(t_buff *buff)
{
	write(buff->fd, buff->str, buff->curr_idx);
	buff->char_written += buff->curr_idx;
	buff->curr_idx = 0;
	return (buff->char_written);
}

size_t	add_char(t_buff *buff, char c)
{
	if (buff->curr_idx >= PF_BUFF_SIZE)
		write_buffer(buff);
	buff->str[buff->curr_idx] = c;
	buff->curr_idx++;
	return (1);
}

size_t	add_str(t_buff *buff, char *s)
{
	size_t i;

	i = 0;
	while (s[i])
	{
		add_char(buff, s[i]);
		i++;
	}
	return (i);
}
