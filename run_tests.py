#!/usr/bin/env python3
import sys
import os

# Globals options
opponents = [
    'abanlin.filler',
    'carli.filler',
    'champely.filler',
    'grati.filler',
    'hcao.filler',
    'superjeannot.filler'
]
games_numbers = 5

# Parsing args
def main():
    try:
        f = open("./resources/players/glodi.filler")
    except FileNotFoundError:
        print("Error: file 'glodi.filler' not found in './ressources/players/'")
        return
    print_header_result()
    for opponent in opponents:
        run_tests(opponent)

# Test
def run_tests(opponent):
    # As p1
    run_test(opponent, 1, "map00")
    run_test(opponent, 2, "map00")
    run_test(opponent, 1, "map01")
    run_test(opponent, 2, "map01")
    
def run_test(opponent, player_nb, map_name):
    win = 0
    loose = 0
    error = 0
    games_played = 0
    player1 = "glodi.filler" if (player_nb == 1) else opponent 
    player2 = "glodi.filler" if (player_nb == 2) else opponent 
    for i in range(0, games_numbers):
        os.system("./resources/filler_vm"
            + " -f ./resources/maps/" + map_name
            + " -p1 ./resources/players/" + player1
            + " -p2 ./resources/players/" + player2
            + " -q > /dev/null")
        try:
            f = open("filler.trace")
        except:
            error += 1;
            print("Error opening filler.trace")
            sys.exit()
        if "glodi.filler won" in f.read():
            win += 1
        elif opponent + " won" in f.read():
            loose += 1
            print("Invalid filler.trace")
            print(f.read())
            sys.exit()
        else:
            error += 1
        games_played += 1
        print_result(player_nb, map_name, win, loose, error, opponent)
    print()

def print_header_result():
    base_format = "| {:>8} | {:>10} | {:>5} | {:>3} | {:>5} | {:>5} | {:>5} |"
    print("\033[1m" + base_format.format(
        "Play as",
        "Opponent",
        "Map",
        "Win",
        "Loose",
        "Error",
        "Ratio"
    ) + "\033[0m")

def print_result(player_nb, map_name, win, loose, error, opponent):
    base_format = "| {:>8} | {:>10} | {:>5} | {:>3} | {:>5} | {:>5} | {:>5} |"
    opponent = opponent.split(".")[0]
    opponent = (opponent[:8] + "..") if len(opponent) > 8 else opponent
    ratio = int(win / (win + loose) * 100) if win + loose > 0 else 0
    print("\x1b[2K" + base_format.format(
        "p" + str(player_nb),
        opponent,
        map_name,
        str(win),
        str(loose),
        str(error),
        get_color(win, win + loose) + str(ratio) + "%\033[0m"
    ), end="\r")

def get_color(win, games_played):
    if win == games_played:
        return "\033[92m"
    elif win / games_played > 0.66:
        return "\033[93m"
    else:
        return "\033[91m"


try:
    #print_result(1, 1, 1, 1, 1)
    main()
except KeyboardInterrupt:
    print("Stop...")
