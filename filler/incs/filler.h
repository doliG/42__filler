/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glodi <glodi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 10:36:56 by glodi             #+#    #+#             */
/*   Updated: 2019/01/10 17:12:27 by glodi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# define PLAYER_NAME "glodi<3"
# define C_RED "\e[31m"
# define C_GRN "\e[32m"
# define C_YEL "\e[33m"
# define C_RES "\e[39m"

# include "libft.h"
# include "ft_printf.h"
# include <stdlib.h>
# include <math.h>

typedef struct {
	int		b_x_size;
	int		b_y_size;
	char	**board;
	char	**old_board;
	int		**heat_map;

	int		p_x_size;
	int		p_y_size;
	int		p_x_offset;
	int		p_y_offset;
	char	**piece;

	char	me;
	char	opponent;
}			t_game;

/*
** Parsing
*/
int			process_parsing(t_game *game, char *line);
int			set_player(t_game *game, char *line);
int			set_board(t_game *game, char *line);
char		**get_board(t_game *game, char *line);
int			set_piece(t_game *game, char *line);
char		**get_piece(t_game *game, char *line);

/*
** Algo
*/
int			set_heat_map(t_game *game);
int			update_heat_map(t_game *game, int ox, int oy);
void		process_algo(t_game *game, int *best_x, int *best_y);
int			is_putable(t_game *game, int x, int y);
int			get_heat(t_game *game, int x, int y);

/*
** Utils
*/
void		reset_game(t_game *game);
void		ft_arrfree(char ***arr);
char		**exit_board(char **board, char **words, char *line);
char		**exit_piece(char **board, char **piece, char *line);
int			my_sqrt(int nb);
int			error_parsing(void);

#endif
